<img src="../images/IDSNlogo__3_.png" width="200"/> 

# Hands-on Lab: Deploy an app on IBM Cloud CLI  
**Estimated time needed:** 30 minutes

# Lab Overview

In this hands-on lab, you will learn how to deploy an application using the IBM Cloud Shell.

![Demo](images/demo.gif)

# Objectives

After completing this exercise, you should be able to perform the following tasks:

- Create an IBM Cloud application by using one of the available runtimes.
- Create a Natural Language Understanding service.
- Sign on to IBM Cloud from the CLI.
- Deploy an application from a local workstation by using the IBM Cloud CLI.
- Test the application with its endpoint after the application is deployed and started.

# Lab Instructions

This lab is divided into the following tasks:

1. Create an IBM Cloud Account
2. Create a Natural Language Understanding service.
3. Modifying and deploying your app on IBM Cloud CLI


## Exercise 1: Create an IBM Cloud Account

Login to IBM Cloud with the account you created in [Lab 2: Getting Started with IBM Cloud](https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBM-CC0100EN-SkillsNetwork/labs/IBMCloud_accountCreation/CreateIBMCloudAccount.md.html)


## Exercise 2: Create an NLU service

1. Navigate to [https://cloud.ibm.com/catalog](https://cloud.ibm.com/catalog) to launch the IBM Cloud Catalog.

2. From the catalog, choose the **Services** menu on the left, and filter by **AI / Machine Learning** and **Lite**. Then click on the **Natural Language Understanding** tile.

![1-catalog](images/1-catalog-nlu.png) { width=1024 height=1024 }

3. On the next screen, select the region closest to you and the **Lite** tier to use the service for free. You can optionally rename the service. Click the **Create** button to continue.

<img src='images/2-catalog-nlu-confirm.png'/>

4. You will be taken to the NLU home page. 

<img src='images/10-nlu-homepage.png'/>

5. You can go to the [resources page](https://cloud.ibm.com/resources). You can confirm your service was created under **Services**.

<img src='images/12-services-nlu.png'/>

6. Open NLU service from resource page under **Services**. Go to Manage and copy **API KEY** and **URL** and save it.

<img src='images/nlu_1.png' alt="nlu_1"/>

## Exercise 2:Modifying and deploying your app on IBM Cloud CLI

1. Click IBM Cloud Shell, as shown in the image below.

<img src='images/Figure1-9.png' alt="Figure1-9"/>

2. Confirm that the CLI runs correctly by running the following command. The output will be as in seen the picture below.

```
ibmcloud version
```
<img src='images/ibm_1.png' alt="ibm_1.png"/>

3. Clone the git repo that contains the sample application:

```
git clone https://github.com/IBM/natural-language-understanding-code-pattern.git
```
<img src='images/ibm_2.png' alt="ibm_2.png"/>


4. Run the command `cd natural-language-understanding-code-pattern` and verify the content of the directory with `ls`.

<img src='images/ibm_3.png' alt="ibm_3.png"/>


5. Run the command ` cp .env.example .env` to create a .env file.

![env_1](images/env_1.PNG)

6. Modify the source code to produce your changes. Open the .env file in the nano text editor using the following command.Using the cursor keys to navigate,and update the NLU API KEY and URL saved earlier.

```
nano .env
```
![env](images/env.PNG)

7. Press CTRL+O to save the file. Verify that the File Name to Write is .env, and then press Return or Enter.

![env_3](images/env_3.PNG)

Press CTRL+X to exit the nano editor.

8. Open the manifest.yml file in the nano text editor using the following command.

```
nano manifest.yml
```
9. Using the cursor keys to navigate, change the application name from **natural-language-understanding-code-pattern** to the **unique** name, change buildpack to **nodejs_buildpack**, change memory to 64MB

![manifest](images/manifest.PNG)

> Note: application name should be changed to the unique name to complete the lab.

10. Press CTRL+O to save the file. Verify that the File Name to Write is manifest.yml, and then press Return or Enter.

![manifest_1](images/manifest_1.PNG)

11. Press CTRL+X to exit the nano editor.

12. Run the command to install cra:

```
 ibmcloud plugin install cra
```

![cra](images/cra.png)

13. Log in to the IBM Cloud with the following command.

```
ibmcloud login -u <your email>
```
![login](images/login.png)

14. Next, use **ibmcloud target** to select the region, organization and space to which you deploy your application. User **-r** to set the region. Use **-o** to specify the organization, which is the email that you used to register to IBM Cloud. Use **-s** to specify the space, which is dev.

```
ibmcloud target -r eu-gb -o **<your_email>** -s dev
```

> Your region might be different depending on which region you created you account in.

15. You can verify if it has been set up correctly by running the following command

```
ibmcloud account orgs
```

16. Run the following command to push the contents of the current directory:

```
ibmcloud app push
```

<img src="images/ibm_push.png"/>

17. Copy and paste that url in your browser to see that app.

<img src="images/app-url.png"/>

Next, click on the Analyze button to analyze the text.

You should see something like this:

<img src="images/11-run-app.png"/>

Next, click on the URL button and enter in www.espn.com. Click on Analyze.

You should see something like this:

<img src="images/12-url.png"/>

Lastly, click on the JSON button to view the results as JSON. This will show you how the Watson Natural Language Understanding service delivers results via API.

You should see something like this:

<img src="images/13-json.png"/>


Congratulations! You've successfully deployed an AI application


<a class="twitter-share-button"
  href="https://twitter.com/intent/tweet?text=I%20just%20learned%20how%20to%20deploy%20a%20cloud%20application%20using%20IBM%20Cloud%20shell%20as%20part%20of%20the%20IBM%20Cloud%20Essentials%20Course.%20%23ibm%20%23ibmcloud"><img src="https://abs.twimg.com/errors/logo46x38.png"/>Tweet and share your achievement!</a>


## Author(s)

Pallavi Rai

 
 ## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 01-02-2022 | 1.0 | Pallavi Rai | Created lab application deployment using IBM CLoud shell |




